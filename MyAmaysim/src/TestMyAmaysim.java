// ****************************** READ ME ************************************************
// Selenium Webdriver is used in this test
// Some of the codes are imported from Selenium IDE
// Precondition before running this test � Selenium webdriver, eclipse, SVN and Junit are installed in your computer
// Mozilla Firefox is the browser used for this testing


import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestMyAmaysim {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.amaysim.com.au/my-account/my-amaysim/login";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testMyAmaysim() throws Exception {
	
	//Launch the website
    driver.get("https://www.amaysim.com.au/my-account/my-amaysim/login");
    
    //Login
    driver.findElement(By.id("my_amaysim2_user_session_login")).clear();
    driver.findElement(By.id("my_amaysim2_user_session_login")).sendKeys("0468827174");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("theHoff34");
    driver.findElement(By.id("login_button")).click();
    
    // Go to My Settings page
    driver.findElement(By.xpath("//ul[@id='menu_list']/li[9]/a/span")).click();
    
    // Assert that Call Forwarding exist in My Settings page
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Call forwarding"));
   
    // Click Edit button in Call Forwarding
    driver.findElement(By.id("edit_settings_call_forwarding")).click();
    
    // Validate Call Forwarding pop-up page
    String confirmation = "You can forward calls to any Australian number. You'll be charged the standard call rate plus a 20c per minute surcharge. E.g. Forwarding your mobile number to a landline/other mobile in Oz costs 12c/min (standard call rate) + 20c/min surcharge = 32c/min. ";
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches(confirmation));
    
    // Condition 1 - Call Forwarding edit is cancelled
    // Click Cancel
    driver.findElement(By.xpath("(//a[contains(text(),'Cancel')])[2]")).click();
    
    // Validate that page is directed back to My Settings page
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("My Settings"));
    
    // Click Edit button in Call Forwarding
    driver.findElement(By.id("edit_settings_call_forwarding")).click();
    
    // Validate Call Forwarding pop-up page
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches(confirmation));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Confirmation"));
    
    // Click Confirm button
    driver.findElement(By.linkText("Confirm")).click();
    
    // Condition 2 - Call Divert is Yes
    // Click Yes radio button
    driver.findElement(By.id("my_amaysim2_setting_call_divert_true")).click();
    // Validate call forwards to field
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Forward calls to"));
    
    // Click Save button
    driver.findElement(By.name("commit")).click();
    
    // Validate Call Forwarding changes is successful
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Success"));
    
    // Close the success popup page
    driver.findElement(By.xpath("(//a[contains(text(),'�')])[6]")).click();
    
    
    // Validate Call Forwarding is set to Yes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Yes"));
    
    // Condition 2 - Call Divert is No
    // Click Edit button in Call Forwarding
    driver.findElement(By.id("edit_settings_call_forwarding")).click();
    
    // Validate Call Forwarding pop-up page
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches(confirmation));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Confirmation"));
    
    // Click Confirm button
    driver.findElement(By.linkText("Confirm")).click();
    
    // Click No radio button
    driver.findElement(By.id("my_amaysim2_setting_call_divert_false")).click();
    driver.findElement(By.xpath("//form[@id='update_call_forwarding_form']/div[2]/div/label[2]/span")).click();
    
    // Click Save button
    driver.findElement(By.name("commit")).click();
    
    // Validate Call Forwarding changes is successful
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("Success"));
    
    // Close the success popup page
    driver.findElement(By.xpath("(//a[contains(text(),'�')])[6]")).click();
    
    // Validate Call Forwarding is set to Yes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("No"));
    
    // Close the page
 	driver.close();
  }
  	

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
